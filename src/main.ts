import noble = require("noble");
import rp = require("request-promise-native");
import { MongoClient } from "mongodb";
import os = require("os");

// Internal imports
import { parseEstimoteTelemetryPacket } from "./libs/estimote_parser";
import { DbData, TelemetryPacket } from "./libs/interfaces";

const HOSTNAME = os.hostname();
const ESTIMOTE_SERVICE_UUID = "fe9a";
const DB_URL = "mongodb://localhost:27027/beacons";
const BACKEND_URI = "https://enablekmusrv.azurewebsites.net/api/beaconpost";

let last_discoverie = 0;
let last_telemetry_packet: TelemetryPacket = {
  shortIdentifier: undefined,
  frameType: undefined,
  subFrameType: undefined,
  protocolVersion: undefined,
  errors: undefined
};

const insertDocuments = (db, data: DbData, callback) => {
  const collection = db.collection("documents");
  collection.insertMany([
    data
  ], (err, result) => {
    callback(result);
  });
};

const send_data = ( data: DbData, telemetry_packet: TelemetryPacket ) => {
  const options = {
    method: "POST",
    uri: BACKEND_URI,
    headers: {
        "ZUMO-API-VERSION" : "2.0.0"
    },
    qs : {
      UserToken : "1d598ca193a7",
      machinebeaconid : HOSTNAME,
      orderbeaconid : telemetry_packet.shortIdentifier,
      timestamp : new Date().getTime(),
      jsonValue : JSON.stringify(data),

    }
  };

  rp(options)
    .then((repos) => {
        console.log("Send Data to enable");
    })
    .catch((err) => {
      console.log(err);
    });
};

noble.on("stateChange", state => {
  console.log("state has changed", state);
  if (state == "poweredOn") {
    const serviceUUIDs = [ESTIMOTE_SERVICE_UUID];
    const allowDuplicates = true;
    noble.startScanning(serviceUUIDs, allowDuplicates, error => {
      if (error) {
        console.log("error starting scanning", error);
      } else {
        console.log("started scanning");
      }
    });
  }
});

noble.on("discover", peripheral => {
  const id: string = peripheral.id.toString();
  const data_object: DbData = {};
  const data = peripheral.advertisement.serviceData.find(el => {
    return el.uuid == ESTIMOTE_SERVICE_UUID;
  }).data;
  const telemetry_packet: TelemetryPacket = parseEstimoteTelemetryPacket(data);

  if (data != undefined && telemetry_packet != undefined) {
    if (last_discoverie < new Date().getTime() / 1000 - 2 ||
      (telemetry_packet.shortIdentifier != last_telemetry_packet.shortIdentifier ||
        telemetry_packet.subFrameType != last_telemetry_packet.subFrameType)) {

      data_object[id] = Array.prototype.slice.call(data, 0);

      MongoClient.connect(DB_URL, (err, db) => {
        if (!err) {
          insertDocuments(db, data_object, () => {
            console.log("DB entry added");
            db.close();
          });
        } else {
          console.log(err);
        }
      });
      send_data( data_object, telemetry_packet );

      last_discoverie = new Date().getTime() / 1000;
      last_telemetry_packet = telemetry_packet;
    }
  }
});
