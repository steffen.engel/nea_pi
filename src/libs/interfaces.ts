export interface DbData {
    [key: string]: Array<string>;
  }

export  interface TelemetryPacket {
    shortIdentifier: string;
    frameType: string;
    subFrameType: string;
    protocolVersion: number;
    errors: any;
    batteryVoltage?: number;
    batteryLevel?: number;
    acceleration?: {
      x: number;
      y: number;
      z: number;
    };
    magneticField?: {
      x: number;
      y: number;
      z: number;
    };
    ambientLightLevel?: number;
    temperature?: number;
    uptime?: {
      number: number;
      unit: any;
    };
    isMoving?: boolean;
    motionStateDuration?: {
      previous: {
        number: number;
        unit: any;
      };
      current: {
        number: number;
        unit: any;
      };
    };
    pressure?: any;
    gpio?: {
      pin0: string;
      pin1: string;
      pin2: string;
      pin3: string;
    };
  }