## Allow node to change bluetooth without root/sudo

```bash
sudo setcap cap_net_raw+eip $(eval readlink -f `which node`)
```

## Use npm to manage node_modules


Install node and mongodb

```bash
sudo apt-get install mongodb-server
sudo apt install libudev-dev


curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs

npm install
mkdir mongoDB

crontab -e
@reboot bash /home/pi/Documents/nea_pi/dist/main.js &
```