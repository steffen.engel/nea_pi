"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const noble = require("noble");
const rp = require("request-promise-native");
const mongodb_1 = require("mongodb");
const os = require("os");
// Internal imports
const estimote_parser_1 = require("./libs/estimote_parser");
const HOSTNAME = os.hostname();
const ESTIMOTE_SERVICE_UUID = "fe9a";
<<<<<<< HEAD
const DB_URL = "mongodb://localhost:27017/beacons";
const BACKEND_URI = "https://enablekmusrv.azurewebsites.net/api/beaconpost";
=======
const DB_URL = "mongodb://localhost:27027/beacons";
>>>>>>> ab592499365a7e757155417cd9b61470c706bd12
let last_discoverie = 0;
let last_telemetry_packet = {
    shortIdentifier: undefined,
    frameType: undefined,
    subFrameType: undefined,
    protocolVersion: undefined,
    errors: undefined
};
const insertDocuments = (db, data, callback) => {
    const collection = db.collection("documents");
    collection.insertMany([
        data
    ], (err, result) => {
        callback(result);
    });
};
const send_data = (data, telemetry_packet) => {
    const options = {
        method: "POST",
        uri: BACKEND_URI,
        headers: {
            "ZUMO-API-VERSION": "2.0.0"
        },
        qs: {
            UserToken: "1d598ca193a7",
            machinebeaconid: HOSTNAME,
            orderbeaconid: telemetry_packet.shortIdentifier,
            timestamp: new Date().getTime(),
<<<<<<< HEAD
            jsonValue: data
=======
            jsonValue: JSON.stringify(data),
>>>>>>> ab592499365a7e757155417cd9b61470c706bd12
        }
    };
    rp(options)
        .then((repos) => {
        console.log("Send Data to enable");
    })
        .catch((err) => {
        console.log(err);
    });
};
noble.on("stateChange", state => {
    console.log("state has changed", state);
    if (state == "poweredOn") {
        const serviceUUIDs = [ESTIMOTE_SERVICE_UUID];
        const allowDuplicates = true;
        noble.startScanning(serviceUUIDs, allowDuplicates, error => {
            if (error) {
                console.log("error starting scanning", error);
            }
            else {
                console.log("started scanning");
            }
        });
    }
});
noble.on("discover", peripheral => {
    const id = peripheral.id.toString();
    const data_object = {};
    const data = peripheral.advertisement.serviceData.find(el => {
        return el.uuid == ESTIMOTE_SERVICE_UUID;
    }).data;
    const telemetry_packet = estimote_parser_1.parseEstimoteTelemetryPacket(data);
    if (data != undefined && telemetry_packet != undefined) {
        if (last_discoverie < new Date().getTime() / 1000 - 2 ||
            (telemetry_packet.shortIdentifier != last_telemetry_packet.shortIdentifier ||
                telemetry_packet.subFrameType != last_telemetry_packet.subFrameType)) {
            data_object[id] = Array.prototype.slice.call(data, 0);
            mongodb_1.MongoClient.connect(DB_URL, (err, db) => {
                if (!err) {
                    insertDocuments(db, data_object, () => {
                        console.log("DB entry added");
                        db.close();
                    });
                }
                else {
                    console.log(err);
                }
            });
            send_data(data_object, telemetry_packet);
            last_discoverie = new Date().getTime() / 1000;
            last_telemetry_packet = telemetry_packet;
        }
    }
});
//# sourceMappingURL=main.js.map